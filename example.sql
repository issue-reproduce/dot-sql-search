-- source: https://github.com/exasol/opendata-examples/blob/master/starter_database/02_sql_queries_and_load_data.sql
select sales_agent_id, sum(software_sales), count(*) number_of_sales
from sales
where sales_date > current_date-30
group by sales_agent_id
order by 2 desc 
limit 10;